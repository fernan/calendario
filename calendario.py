#!/usr/bin/python3

calendar = {}

def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

def get_time(atime):
    """Devuelve todas las actividades para la hora atime como una lista de tuplas"""
    activities = []
    for date, times in calendar.items():
        for t, activity in times.items():
            if t == atime:
                activities.append((date, t, activity))
    activities.sort()
    return activities

def get_all():
    """Devuelve todas las actividade sen el calendario como una lista de tuplas"""
    all_activities = []
    for date, times in calendar.items():
        for t, activity in times.items():
            all_activities.append((date, t, activity))
    all_activities.sort()
    return all_activities

def get_busiest():
    """Devuelve la fecha con más actividades, y su número de actividades"""
    busiest = None
    busiest_no = 0
    for date, times in calendar.items():
        activities_count = len(times)
        if activities_count > busiest_no:
            busiest = date
            busiest_no = activities_count
    return busiest, busiest_no

def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""
    # Aquí deberías implementar la lógica para verificar el formato de la fecha
    return True

def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    # Aquí deberías implementar la lógica para verificar el formato de la hora
    return True

def get_activity():
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    while True:
        date = input("Introduce la fecha (aaaa-mm-dd): ")
        time = input("Introduce la hora (hh:mm): ")
        activity = input("Introduce la actividad: ")
        if check_date(date) and check_time(time):
            return (date, time, activity)
        else:
            print("Error en el formato de la fecha o la hora. Vuelve a intentarlo.")

def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option

def run_option(option):
    """Ejecuta el código necesario para la opción dada"""
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
    elif option == 'B':
        activities = get_all()
        show(activities)
    elif option == 'C':
        busiest, busiest_no = get_busiest()
        print(f"El día más ocupado es {busiest} con {busiest_no} actividades.")
    elif option == 'D':
        atime = input("Introduce la hora (hh:mm): ")
        activities = get_time(atime)
        show(activities)

def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()
